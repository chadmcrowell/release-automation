# release-automation
release automation for changelog and release notes

## How to run script
- If Python is not installed:
    - `brew install python`
- Install required packages:
    - `pip3 install -r create-release-note/requirements.txt`

- Export 3 environment variables to create a test markdown file with release notes from promtail repo on BB:
    *Note: the git username and token must have repo1 access*
    
    - `export GIT_USER=<git_username>`
    - `export GIT_TOKEN=<repo1_git_token>`
    - `export REL_VER="6.15.3-bb.0`

- Run Script:
    - `python3 create-release-note/create-release-note.py`

- After the release_note.md file is generated, check the content:

    - `cat release_note.md`
