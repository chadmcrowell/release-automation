### Updated

- Updated ironbank/opensource/grafana/promtail v2.9.1 -> v2.9.2
- Updated registry1.dso.mil/ironbank/opensource/grafana/promtail v2.9.1 -> v2.9.2
- Updated chart version to 6.15.3


