import os
import logging
import keepachangelog
import subprocess
import re

logging.basicConfig(level=logging.INFO)
rel_ver = "{}".format(os.environ.get("REL_VER"))

repo_urls = {
    "promtail": "https://repo1.dso.mil/big-bang/product/packages/promtail.git",
}

def get_repo_url(key):
    url = repo_urls.get(key, "")
    if not url:
        return ""
    
    git_user = os.environ.get("GIT_USER", "")
    git_token = os.environ.get("GIT_TOKEN", "")
    
    return url.replace("https://", f"https://{git_user}:{git_token}@")

def write_packages_version(repo="docs"):
    docs_repo_url = get_repo_url(repo)
    subprocess.run(["git", "clone", "--depth", "1", docs_repo_url, "git-temp"])
    
    content = read_markdown_file()
    packages_table = extract_table_from_markdown(content)
    write_table_to_markdown(packages_table)
    subprocess.run(["rm", "-rf", "git-temp"], check=True)
    
def read_markdown_file(filepath="git-temp/release_template.md"):
    with open(filepath, 'r') as file:
        return file.read()
    
def extract_table_from_markdown(content):
    # Find the table using regex
    table_pattern = r"\|.*?\|\n(\|[-:| ]+\|)*\n(\|.*?\|\n)+"
    match = re.search(table_pattern, content, re.DOTALL)
    
    if not match:
        return None

    table_string = match.group(0)
    rows = table_string.strip().split("\n")
    
    # Split each row into its cells
    parsed_table = [row.split("|")[1:-1] for row in rows if row]
    for row in parsed_table:
        for i, cell in enumerate(row):
            row[i] = cell.strip()

    return parsed_table

def write_table_to_markdown(parsed_table, output_filepath="release_note.md"):
    with open(output_filepath, 'w') as file:
        file.write(f"# Release Notes - {rel_ver}\n\n")
        for row in parsed_table:
            row_content = " | ".join(row)
            file.write(f"| {row_content} |\n")
            # If the row is the header-divider (like: |---|---|---|), add an extra newline for formatting
            if all(cell.strip().count("-") == len(cell.strip()) for cell in row):
                file.write("\n")
        file.write("\n\n")

def main():
    #changelog_repos = os.environ.get("CHANGELOG_REPOS").split(",")
    
    #write_packages_version()

    combined_entries = {
        "Added": [],
        "Changed": [],
        "Fixed": [],
        "Removed": [],
        "Updated": []
    }

    for repo_name in repo_urls.keys():
        changelog_repo_url = get_repo_url(repo_name)
        
        if changelog_repo_url:
            subprocess.run(["git", "clone", "--depth", "1", changelog_repo_url, "git-temp"])
            
            try:
                changes = keepachangelog.to_dict("git-temp/CHANGELOG.md", show_unreleased=True)
            except Exception as e:
                logging.info(f"Error processing changelog for {repo_name}: {e}")
                continue

            if rel_ver in changes:
                for section in combined_entries.keys():
                    if section.lower() in changes[rel_ver]:
                        combined_entries[section].extend(changes[rel_ver][section.lower()])

            logging.info(f"Compiled changelog entries for {repo_name}")
            subprocess.run(["rm", "-rf", "git-temp"], check=True)
        else:
            print(f"No matching repository found for {repo_name}")
            exit(1)

    with open("release_note.md", "a") as output_file:
        for section, entries in combined_entries.items():
            if entries:
                output_file.write(f"### {section}\n\n")
                for entry in entries:
                    output_file.write(f"- {entry}\n")
                output_file.write("\n\n")

if __name__ == "__main__":
    main()

